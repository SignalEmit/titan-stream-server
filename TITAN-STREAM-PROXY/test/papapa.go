package main

import (
	rabiitmq "TITAN-STREAM-PROXY/public/rabbitmq"
	"fmt"
	"strconv"
)

func main() {
	// 创建消息队列收数据
	client := &rabiitmq.RabbitMQPublishSubscribeClient{
		User: "guest",
		Pwd:  "guest",
		IP:   "127.0.0.1",
		Port: "5672",
	}

	//size := (20*1024 + 4)
	count := 1
	stream := make(chan []byte)
	go func() {
		for bytes := range stream {
			bytes[0] = 1
			count += 1
			str := strconv.Itoa(count)
			fmt.Printf(str, "\n")

		}
	}()

	go client.RecvMsg("codec",
		"22", stream)
	select {}
}
