package server

type RabbitMQConfig struct {
	User string
	Pwd  string
	IP   string
	Port string
}

type RabbitmqQueueConfig struct {
	Exchange  string
	QueueName string
}
