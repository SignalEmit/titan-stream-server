/*
 * @Description:
 * @Version: 1.0
 * @Autor: ycb
 * @Date: 2022-03-22 08:42:25
 * @LastEditors: ycb
 * @LastEditTime: 2022-04-08 08:46:45
 */
package main

import (
	"TITAN-STREAM-PROXY/public/system"
	"TITAN-STREAM-PROXY/server"
	"flag"
	"fmt"
	"log"
	"os"
)

var rabbitmq_user string
var rabbitmq_pwd string
var rabbitmq_ip string
var rabbitmq_port string
var hls_port string
var flv_port string

//绑定参数变量
func init() {
	flag.StringVar(&rabbitmq_user, "rabbitmq_user", "guest", "rabbitmq 用户名")
	flag.StringVar(&rabbitmq_pwd, "rabbitmq_pwd", "guest", "rabbitmq 密码")
	flag.StringVar(&rabbitmq_ip, "rabbitmq_ip", "127.0.0.1", "rabbitmq ip")
	flag.StringVar(&rabbitmq_port, "rabbitmq_port", "5672", "rabbitmq 端口")
	flag.StringVar(&hls_port, "hls_port", "8000", "hls 端口")
	flag.StringVar(&flv_port, "flv_port", "3333", "flv 端口")

	flag.Usage = func() {
		fmt.Fprintf(os.Stderr, "Usage of params:\n")
		flag.PrintDefaults()
	}
}

func main() {

	//解析参数
	flag.Parse()

	log.Printf("rabbitmq connect: %s:%s@%s:%s", rabbitmq_user, rabbitmq_pwd, rabbitmq_ip, rabbitmq_port)

	ip_map, err := system.GetInterfacesIP()
	if err != nil {
		log.Printf("%s: %s", "Failed to get all ips", err)
		return
	}

	str_ips := ""
	//print(ip_map)
	for _, value := range ip_map {
		str_ips += "[" + value + "]"
	}

	rabbit_config := server.RabbitMQConfig{
		User: rabbitmq_user,
		Pwd:  rabbitmq_pwd,
		IP:   rabbitmq_ip,
		Port: rabbitmq_port,
	}

	hls_rabbitmq_queue_config := server.RabbitmqQueueConfig{
		Exchange:  "HLS",
		QueueName: "HLS-recv-node-" + str_ips,
	}

	hls_client := &server.HLS{
		HLS_Port:            ":" + hls_port,
		RabbitmqQueueConfig: hls_rabbitmq_queue_config,
		RabbitConfig:        rabbit_config,
	}

	go hls_client.Start()

	flv_rabbitmq_queue_config := server.RabbitmqQueueConfig{
		Exchange:  "FLV",
		QueueName: "FLV-recv-node-" + str_ips,
	}

	flv_client := &server.FLV{
		FLV_Port:            ":" + flv_port,
		RabbitmqQueueConfig: flv_rabbitmq_queue_config,
		RabbitConfig:        rabbit_config,
	}

	go flv_client.Start()

	select {}
}
