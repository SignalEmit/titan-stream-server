package hex

func Hex2uint64(hexs []byte) (uint64, bool) {
	if len(hexs) == 8 {
		var x uint64
		count := 1
		for i := 7; i >= 0; i-- {
			x += (uint64)(hexs[i]) << (64 - 8*count)
			count++
		}

		return x, true
	}
	return 0, false

}
