/*
 * @Description: id工厂
 * @Version: 1.0
 * @Autor: ycb
 * @Date: 2022-03-30 18:29:26
 * @LastEditors: ycb
 * @LastEditTime: 2022-03-30 18:48:59
 */
package idfactory

import (
	"math/rand"
	"time"
)

func GetIDForString(str_len int) string {
	str := "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ"
	bytes := []byte(str)
	result := []byte{}
	r := rand.New(rand.NewSource(time.Now().UnixNano()))
	for i := 0; i < str_len; i++ {
		result = append(result, bytes[r.Intn(len(bytes))])
	}
	return string(result)
}
