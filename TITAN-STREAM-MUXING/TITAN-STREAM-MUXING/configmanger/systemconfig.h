#pragma once
#include <string>
#include <configbase.h>
#include <jsoncpp/json.h>
#include <list>
#include <log.h>

namespace Config 
{
	struct Rabbitmq 
	{
		std::string user;
		std::string password;
		std::string ip;
		int port;
	};

	struct Stream
	{
		std::string url;
		std::string hls_route;
		std::string flv_route;
	};


	struct HLSConfig
	{
		int ts_time = 1000;
		int ts_count = 2;
		int ts_count_max = 70;
	};

	struct SystemConfigParam
	{
		//mq配置
		Rabbitmq rabbitmq_config;
		// hls配置
		HLSConfig hls_config;
		// 码流地址
		std::list<Stream> stream_list;
	};

	class SystemConfig : public ConfigBase
	{
	public:
		
		SystemConfig(std::string filepath);
		
		~SystemConfig();
	
		virtual bool paserData();
		
		SystemConfigParam getSystemConfigParam();

	protected:

		SystemConfigParam m_system_data;
	};
}