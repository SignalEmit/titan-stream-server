#include "systemconfig.h"

namespace Config
{
	
	SystemConfig::SystemConfig(std::string filepath)
		:ConfigBase(filepath) 
	{

	} 

	
	SystemConfig::~SystemConfig()
	{

	}

	
	SystemConfigParam SystemConfig::getSystemConfigParam()
	{
		return m_system_data;
	}

	bool SystemConfig::paserData()
	{
	
		Json::Reader reader;
		Json::Value json_object;
		if (!reader.parse(m_file_data.data(), json_object))
		{
			Log::printError("json解析失败");
			return false;
		}
		else
		{
			if (!json_object.isMember("rabbitmq"))
			{
				Log::printError("json解析失败<rabbitmq配置> ");
				return false;
			}

			if (!json_object.isMember("hls_config"))
			{
				Log::printError("json解析失败hls配置失败 ");
				return false;
			}


			if (!json_object.isMember("stream_array"))
			{
				Log::printError("json解析失败stream_array ");
				return false;
			}

			//Json::Value json_rabbitmq = json_object["rabbitmq"];
			if (!json_object["rabbitmq"].isMember("user"))
			{
				Log::printError("json解析失败 rabbitmq->enable ");
				return false;
			}

			if (!json_object["rabbitmq"].isMember("password"))
			{
				Log::printError("json解析失败rabbitmq->password");
				return false;
			}

			if (!json_object["rabbitmq"].isMember("ip"))
			{
				Log::printError("json解析失败rabbitmq->ip");
				return false;
			}

			if (!json_object["rabbitmq"].isMember("port"))
			{
				Log::printError("json解析失败rabbitmq->port");
				return false;
			}


			m_system_data.rabbitmq_config.user = json_object["rabbitmq"]["user"].asString();
			m_system_data.rabbitmq_config.password = json_object["rabbitmq"]["password"].asString();
			m_system_data.rabbitmq_config.ip = json_object["rabbitmq"]["ip"].asString();
			m_system_data.rabbitmq_config.port = json_object["rabbitmq"]["port"].asInt();



			if (!json_object["hls_config"].isMember("ts_time"))
			{
				Log::printError("json解析失败hls_config->ts_time");
				return false;
			}

			if (!json_object["hls_config"].isMember("ts_count"))
			{
				Log::printError("json解析失败hls_config->ts_count");
				return false;
			}

			if (!json_object["hls_config"].isMember("ts_count_max"))
			{
				Log::printError("json解析失败hls_config->ts_count_max");
				return false;
			}

			m_system_data.hls_config.ts_time = json_object["hls_config"]["ts_time"].asInt();
			m_system_data.hls_config.ts_count = json_object["hls_config"]["ts_count"].asInt();
			m_system_data.hls_config.ts_count_max = json_object["hls_config"]["ts_count_max"].asInt();



			Json::Value json_streamarray = json_object["stream_array"];
			for (auto i = 0; i < json_streamarray.size(); i++)
			{
				Json::Value &stream = json_streamarray[i];
				if (!stream.isMember("url"))
				{
					Log::printError("json解析失败 stream_array["+
						std::to_string(i)
						+"]->url ");
					return false;
				}

				if (!stream.isMember("hls_route"))
				{
					Log::printError("json解析失败 stream_array[" +
						std::to_string(i)
						+ "]->hls_route ");
					return false;
				}

				if (!stream.isMember("flv_route"))
				{
					Log::printError("json解析失败 stream_array[" +
						std::to_string(i)
						+ "]->flv_route ");
					return false;
				}

				Stream node;
				node.url = stream["url"].asString();
				node.hls_route = stream["hls_route"].asString();
				node.flv_route = stream["flv_route"].asString();
				

				
				m_system_data.stream_list.push_back(node);

			}
			
		}

		return true;
	}

}