#pragma once
#include <string>



namespace System
{ 
	class Path
	{ 
	public:
		static std::string getRunningPath();
		// 获取系统分割符
		static std::string getSubSymbol();
	};
}