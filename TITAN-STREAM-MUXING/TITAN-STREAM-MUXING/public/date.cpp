#include "date.h"
namespace Date
{
	Date::Date()
	{

	}

	Date::~Date()
	{

	}

	std::string Date::getNowTime()
	{
		struct tm t;   //tm结构指针
		time_t now;  //声明time_t类型变量
		time(&now);      //获取系统日期和时间
		localtime_s(&t, &now);   //获取当地日期和时间


		return std::to_string(t.tm_year + 1900) + "-" + std::to_string(t.tm_mon) + "-" + std::to_string(t.tm_mday) + " " +
			std::to_string(t.tm_hour) + ":" + std::to_string(t.tm_min) + ":" + std::to_string(t.tm_sec);
	}

}