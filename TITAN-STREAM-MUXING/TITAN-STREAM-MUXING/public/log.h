#pragma once
#include <string>
#include <iostream>
#include "date.h"
class Log
{
public:
	Log();
	~Log();
	// 设置打印日志
	static bool setPrint(bool state);
	// 日志打印
	static void print(std::string str);
	// 日志打印
	static void printInfo(std::string str);

	// 日志打印
	static void printError(std::string str);

	// 日志打印
	static void printDebug(std::string str);
private:
	// 打印日志
	static bool m_print_log;
};