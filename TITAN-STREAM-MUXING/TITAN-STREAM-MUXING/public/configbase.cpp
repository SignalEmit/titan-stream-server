#include "configbase.h"
// 定义一些个系统相关的一些接口
namespace Config
{
	// 配置文件读写构造函数
	ConfigBase::ConfigBase(std::string filepath):m_filepath(filepath)
	{

	} 
	 
	// 配置文件读写
	ConfigBase::~ConfigBase()
	{

	}


	// 打开文件
	bool ConfigBase::open()
	{
		File::File file(m_filepath);
		if (file.isExist())
		{
			m_file_data = file.readAll();
			return true;
		}
		return false;
	}

	// 解析
	bool ConfigBase::paser()
	{
		// 文件判空 如果没读到内容不进行解析
		if (!this->open())
		{
			return false;
		}

		return this->paserData();
	}

}