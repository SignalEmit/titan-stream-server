#pragma once
#include <string>
#include <ctime>

namespace Date
{
	class Date
	{
	public:
		static std::string getNowTime();
	private:
		Date();
		~Date();
	};
}