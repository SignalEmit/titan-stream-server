#pragma once
#include <functional>
#include "./common/mongoose.h"
#include <string>
#include <atomic>
#include <thread>
#include <map>
#include "../log.h"
#include "httptype.h"

namespace Http
{
	// httpserver回执层
	class HttpServerReply
	{ 
	public:
		HttpServerReply(mg_connection *connection, http_message *http_req);
		~HttpServerReply();
		// http回执层，类似nodejs的end函数
		bool end(HTTPSTATECODE state_code,char *data, size_t len);
		bool sendM3U8(HTTPSTATECODE state_code, char* data, size_t len);
		bool sendTS(HTTPSTATECODE state_code, char* data, size_t len);
		// 重载运算符
		HttpServerReply &operator = (const HttpServerReply &);
	private:
		// 连接
		mg_connection *p_connection = nullptr;
		// 消息指针
		http_message *p_http_req = nullptr;
	};

	using http_server_reply = std::function<void(HttpServerReply,std::string, char *,size_t )>;

	class HttpServer
	{
	public:
		HttpServer();
		HttpServer(int port);
		~HttpServer();
		// 注册回执回调
		void resigterReplyCallBack(http_server_reply);
		// 监听
		bool listen(int port = -1);
		// 停止
		bool stop();
	protected:
		static void httpCallBack(mg_connection *connection, int event_type, void *event_data);
		//获取消息，相当于开始监听了
		void getAllNetMessage();
	private:

		int m_port = -1;
		// 连接管理器
		mg_mgr m_mgr;

		// 线程专用原子数据
		std::atomic<bool> m_thread_running;
		// 线程
		std::thread *p_thread = nullptr;
		// http的回执回调
		http_server_reply p_server_reply = nullptr;

	};
}
