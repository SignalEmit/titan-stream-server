#pragma once
#include <map>
#include <string>
namespace Http
{
	// http状态码
	enum HTTPSTATECODE
	{
		HTTP_STATE_OK,				// 200ok
		HTTP_STATE_NOTFOUND		// 404
	};

	typedef std::string HTTP_HEAD_KEY;
	typedef std::string HTTP_HEAD_VALUE;

	class HttpTypeManger
	{
	public:
		static bool setAllCode();
		 
		static bool getStateString(HTTPSTATECODE code,std::string &strcode);
	private:
		HttpTypeManger();
		~HttpTypeManger();
	private:
		static std::map< HTTPSTATECODE, std::string> m_http_code_map;
	};
	
	

}