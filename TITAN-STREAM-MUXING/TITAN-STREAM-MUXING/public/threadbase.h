#pragma once
#include <thread>
#include <atomic>
#include <functional>
namespace Thread
{
	class ThreadBase
	{
	public:
		ThreadBase();
		~ThreadBase();
		bool start();
		virtual bool stop();
	protected:
		virtual void run() = 0;
	protected:
		std::atomic_bool m_running;
		std::thread *p_thread = nullptr;
	};
 }