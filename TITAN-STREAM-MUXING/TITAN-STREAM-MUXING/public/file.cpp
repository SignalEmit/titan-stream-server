#include "file.h"
// 定义一些个系统相关的一些接口
namespace File
{
	File::File(std::string filepath)
	{
		m_filepath = filepath;
		
	}

	bool File::open(std::string default_param )
	{
		fopen_s(&p_file,m_filepath.data(), default_param.data());
		if (p_file == nullptr)
		{
			return false;
		}
		return true;
	}

	 File::~File()
	{
		 if (p_file != nullptr)
		 {
			 fclose(p_file);
			 p_file = nullptr;
		 }
		  
	}

	 bool File::close()
	 {
		 if (p_file != nullptr)
		 {
			 fclose(p_file);
			 p_file = nullptr;
		 }
		 return true;
	 }

	 bool File::write(const uint8_t* buffer, const size_t& len)
	 {
		 if (nullptr != p_file)
		 {
			 fwrite(buffer, 1, len, p_file);
			 fflush(p_file);
			 return true;
		 }
		 else
		 {
			 return false;
		 }
	 }

	 // 打开文件
	 bool File::isExist( )
	 {
		 std::ifstream f(m_filepath.c_str());
		 return f.good();
	 }

	 std::string File::readAll()
	 {
		 std::string str;
		 std::ifstream ifile(m_filepath.data(), std::ifstream::in | std::ifstream::binary);
		 std::stringstream buffer;
		 buffer << ifile.rdbuf();
		 str = buffer.str();

		 ifile.close();
		 return str;
	 }
}