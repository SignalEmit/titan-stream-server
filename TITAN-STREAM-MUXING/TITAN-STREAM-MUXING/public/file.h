#pragma once
#include <string>
#include <fstream> 
#include <sstream>  

// 配置文件读写基类
namespace File
{

	class File 
	{
	public: 
		// 配置文件读写构造函数
		File(std::string filepath);
		// 配置文件读写
		~File();
		// 打开文件-参数是否创建文件
		bool isExist();
		// 读取数据
		std::string readAll();
		bool write(const uint8_t* buffer, const size_t& len);
		bool open(std::string default_param = "wb");
		bool close();
	protected:
		// 文件
		std::string m_filepath;
		// ifstream 
		std::ifstream m_read_stream;

		FILE* p_file = nullptr;
	};
}