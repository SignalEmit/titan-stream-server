#pragma once

#include <condition_variable>
#include <mutex>
#include <functional>
#include <list>
#include <iostream>

namespace Buffer
{
	template<class T>
	class RingBuffer
	{
	public:
		RingBuffer(uint64_t max_size = 256):m_max(max_size)
		{

		}


		~RingBuffer()
		{ 
			std::unique_lock<std::mutex> lock(m_mutex);
			m_list.clear();
			m_cv.notify_all();
		}

		T& getHeadNode()
		{
			std::unique_lock<std::mutex> lock(m_mutex);
			if (m_list.empty())
			{
				m_cv.wait(lock);
			}
			return m_list.front();
		}

		void popHeadNode()
		{
			std::unique_lock<std::mutex> lock(m_mutex);
			if (!m_list.empty())
			{
				m_list.pop_front();
			}
		}

		std::list<T> getAllData()
		{
			std::unique_lock<std::mutex> lock(m_mutex);
			if (m_list.empty())
			{
				m_cv.wait(lock);
			}
			std::list<T> temp_data = m_list;
			m_list.clear();
			return temp_data;
		}

		// 向后插入一个数据
		bool pushBack(T data)
		{
			// 插入一个数据
			std::unique_lock<std::mutex> lock(m_mutex);
			// 如果队列里的数据大于最大限制啥也不干
			if (m_list.size() > m_max)
			{
				//std::cout << "数据超限制清理一个数据" << m_list.size() <<"  " << m_max << std::endl;
				m_list.pop_front();
				//m_list.clear();
				//return false;
			}
			m_list.push_back(data);
			// 通知消费者消费
			m_cv.notify_one();
			return true;
		}
	private:
		std::list<T> m_list;
		std::mutex m_mutex;
		std::condition_variable m_cv;
		uint64_t m_max = 256;
	};
 }