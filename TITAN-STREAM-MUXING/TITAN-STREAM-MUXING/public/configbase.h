#pragma once
#include <string>
#include "file.h"

// 配置文件读写基类
namespace Config
{
	class ConfigBase
	{
	public:  
		// 配置文件读写构造函数
		ConfigBase(std::string filepath);
		// 配置文件读写
		~ConfigBase();
		// 打开文件
		bool open();
		// 解析
		bool paser();
		// 解析文件
		virtual bool paserData() = 0;

	protected:
		// 文件
		std::string m_filepath;
		// 文件内容
		std::string m_file_data;
	};
}