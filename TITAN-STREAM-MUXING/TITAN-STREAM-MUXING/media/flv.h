#pragma once
#include <list>
#include <memory>
#include "type.h"
#include <iostream>
#include <log.h>
#include <string>
#include <mutex>
#include <queue>
#include <map>
#include <iostream>
#include <chrono>
#include <file.h>
#include "stream_muxing_base.h"
// 导入ffmpeg相关头文件
extern "C"
{
#include "libavdevice/avdevice.h"
#include "libavcodec/avcodec.h"
#include "libswscale/swscale.h"
#include "libswresample/swresample.h"
#include "libavutil/imgutils.h"
}

namespace Media
{
	class FLV : public StreamMuxingBase
	{
		
	public:
		FLV(
			const FLVParam&flv_param,
			AVCodecContext* in_video_codec, 
			AVCodecContext* in_audio_codec);
		~FLV();

		// 塞入数据 true:添加成功 false：失败重试其他
		bool addPacket(const ENUM_MEDIA_TYPE &type, AVPacket* packet);
		// 获取初始化成功还是失败
		bool getInitStatus() 
		{
			return m_init_status;
		}

		virtual bool streamOut2Memory(uint8_t* buf, int buf_size);

		// 获取是否需要重启flv串流
		bool getNeedRestart();
	
	protected:
		
	private:
		//bool paserScript(const uint8_t* data, const int buf_size);
		// 解析出第一帧音频空包
		bool paserFirstAudioPacket(const uint8_t* data, const int buf_size);
	private:
		std::list<std::string> m_m3u8_last_item_list;
		std::map<std::string, std::shared_ptr<StreamPacket>  > m_ts_map;
		FLVParam m_flv_param;
		bool b_is_first = true;
		// 找到了第一帧音频数据
		bool b_find_first_audio_data = false;
		std::shared_ptr<StreamPacket> m_flv_head;
		//File::File* p_file;
		bool b_first_add_packet = true;
		int64_t m_stream_start_time = 0;
		AVRational m_tran_time_base;

		// 判断音频第一帧找的次数
		int m_audio_first_data_count = 0;


		// 文件
		//File::File* file;
	};
}


