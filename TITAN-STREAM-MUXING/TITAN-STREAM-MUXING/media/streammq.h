#pragma once
#include <rabbitmq.h>
#include <threadbase.h>
#include "./type.h"
#include <ringbuffer.h>
#include <log.h>
namespace Media
{
	class StreamMQ : public Thread::ThreadBase
	{
	public:
		StreamMQ() {}
		~StreamMQ()
		{
			this->stop();
			if (p_rabbitmq)
			{
				delete p_rabbitmq;
				p_rabbitmq = nullptr;
			}
		}
		void connect(const std::string& connect_string,
			const std::string& exchange,
			const std::string& queue_name,
			Buffer::RingBuffer<std::shared_ptr<StreamPacket> >* stream_buffer);
		virtual bool stop();
	protected:
		virtual void run();
	private:
		
	private:
		std::string m_exchange;
		std::string m_queue_name;
		std::string m_connect_string;
		Rabbitmq::Rabbitmq *p_rabbitmq = nullptr;
		Buffer::RingBuffer<std::shared_ptr<StreamPacket> >* p_stream_buffer = nullptr;
	};
}
