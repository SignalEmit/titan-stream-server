#pragma once
#include <list>
#include <memory>
#include "type.h"
#include <iostream>
#include <log.h>
#include <string>
#include <mutex>
#include <queue>
#include <map>
#include <iostream>
#include <chrono>
#include <algorithm>
#include "stream_muxing_base.h"
// 导入ffmpeg相关头文件
extern "C"
{
#include "libavdevice/avdevice.h"
#include "libavcodec/avcodec.h"
#include "libswscale/swscale.h"
#include "libswresample/swresample.h"
#include "libavutil/imgutils.h"
}

namespace Media
{
	class HLS : public StreamMuxingBase
	{
		

	public:
		HLS(
			const HLSParam &hls_param,
			AVCodecContext* in_video_codec,
			AVCodecContext* in_audio_codec);
		~HLS();

		// 塞入数据 true:添加成功 false：失败重试其他
		bool addPacket(const ENUM_MEDIA_TYPE &type, AVPacket* packet);
		// 获取初始化成功还是失败
		bool getInitStatus() 
		{
			return m_init_status;
		}
		

	protected:
		virtual bool streamOut2Memory(uint8_t* buf, int buf_size);
	private:
		
		bool createM3U8AndTS(const double& time);
		int getTsIndex();
	private:
	
		bool m_hls_node_cut_start = false;
		
		double m_cut_start_pts = 0;
		double m_cut_end_pts = 0;

	
		std::string m_m3u8_head = "#EXTM3U\n"
			"#EXT-X-VERSION:3\n"
			"#EXT-X-TARGETDURATION:";
		std::string m_m3u8_sequence = "#EXT-X-MEDIA-SEQUENCE:";
		std::list<std::string> m_m3u8_last_item_list;

		// 时间列表
		std::vector<double> m_m3u8_drution_list;

		HLSParam m_hls_param;
		uint64_t m_m3u8_sequence_count = 1;

		int m_ts_index = 1;

	};
}


