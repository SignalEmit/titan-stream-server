#pragma once
#include <list>
#include <memory>
#include "type.h"
#include <iostream>
#include <log.h>
#include <string>
#include <mutex>
#include <queue>
#include <map>
#include <iostream>
#include <chrono>
// 导入ffmpeg相关头文件
extern "C"
{
#include "libavdevice/avdevice.h"
#include "libavcodec/avcodec.h"
#include "libswscale/swscale.h"
#include "libswresample/swresample.h"
#include "libavutil/imgutils.h"
#include <libavutil/timestamp.h>
}

namespace Media
{
	class StreamMuxingBase 
	{
		

	public:
		StreamMuxingBase(
			const std::string &stream_type,
			AVCodecContext* in_video_codec,
			AVCodecContext* in_audio_codec);
		~StreamMuxingBase();
		// 塞入数据 true:添加成功 false：失败重试其他
		virtual bool addPacket(const ENUM_MEDIA_TYPE& type, AVPacket* packet) = 0;
		// 获取初始化成功还是失败
		bool getInitStatus() 
		{
			return m_init_status;
		}

		bool start();
		
	protected:
		bool addStream(AVCodecContext* p_in_codec,int &out_index);
		virtual bool streamOut2Memory(uint8_t* buf, int buf_size) = 0;
	protected:
		std::string m_stream_url;
		// 封装节点
		AVFormatContext* p_out_formatctx = nullptr;
		AVStream* p_out_video_stream = nullptr;
		AVStream* p_out_audio_stream = nullptr;

		AVCodecContext* p_in_video_codec_centext = nullptr;
		AVCodecContext* p_in_audio_codec_centext = nullptr;
		// 视频索引
		int m_video_out_index = -1;
		// 音频索引
		int m_audio_out_index = -1;
		// 获取初始化状态
		bool m_init_status = false;
		uint8_t* p_out_buffer = nullptr;

		std::list<std::shared_ptr<StreamPacket> > m_stream_packet_list;
	
		//uint8_t* p_stream_buffer = nullptr;

	private:
		static int write2Memory(void* opaque, uint8_t* buf, int buf_size);
	
	};
}


