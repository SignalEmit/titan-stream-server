#pragma once
#include <threadbase.h>
#include <string>
#include <map>
#include "type.h"
#include <log.h>
#include <mutex>
#include <mutex>
#include <queue>
#include <memory>
// 导入ffmpeg相关头文件
extern "C"
{
#include "libavdevice/avdevice.h"
#include "libavcodec/avcodec.h"
#include "libswscale/swscale.h"
#include "libswresample/swresample.h"
#include "libavutil/imgutils.h"
#include "libavutil/time.h"
}
#include "type.h"
#include "hls.h"
#include "flv.h"
#include "decoder_encoder.h"

namespace Media
{
	class StreamManger : public Thread::ThreadBase
	{
	public:
		// 析构函数
		StreamManger(const std::string &url,bool  enable_local_ip = false,std::string local_ip = "");
		// 构造函数
		~StreamManger();
		bool setHLSParam(const HLSParam& hls_param);
		bool setFLVParam(const FLVParam& hls_param);
	protected:
		void run();
		void addPacketToMuxing(const ENUM_MEDIA_TYPE& type, AVPacket* packet);
		bool createCodec(AVCodecContext** codec_context, AVStream* stream);
	private:
		// 初始化码流
		bool initMuxingStream(AVCodecContext* video_codec, AVCodecContext* audio_codec);
		// 删除hls
		bool delHls();
		// 初始化hls拉流
		bool initHls(AVCodecContext* in_video_codec, AVCodecContext* in_audio_codec);
		// 删除flv
		bool delFLV();
		// 初始化flv拉流
		bool initFLV(AVCodecContext* in_video_codec, AVCodecContext* in_audio_codec);
	private:
		// 初始化hls配置
		bool m_init_hls_setting = false;
		// hls封装类
		HLS* p_hls_muxing = nullptr;
		// 初始化flv配置
		bool m_init_flv_setting = false;
		// flv封装类
		FLV* p_flv_muxing = nullptr;
		// HLS参数
		HLSParam m_hls_param;
		// FLV参数
		FLVParam m_flv_param;
		// 编解码
		DecoderEncoder* p_decoder_encoder = nullptr;

		AVCodecContext* video_translater_codec = nullptr;
		AVCodecContext* audio_translater_codec = nullptr;

		int64_t m_stream_start_time = 0;

		// 以下三个变量用于将录像或者hls同步成直播 类似-re参数
		int64_t m_last_video_pts = 0;
		int64_t m_last_audio_pts = 0;
		int64_t m_ffmpeg_start_time = 0;


		bool m_video_translate = false;
		bool m_audio_translate = false;
		// url
		std::string m_url;
		bool m_enable_local_ip = false;
		std::string m_local_ip;
		
	};

}
