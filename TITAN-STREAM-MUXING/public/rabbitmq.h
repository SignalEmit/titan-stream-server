#pragma once
#include "AMQPcpp.h"
namespace Rabbitmq
{
	class Rabbitmq
	{
	public:
		Rabbitmq(const std::string &connect_string,
			const std::string &exchange,
			const std::string &queue_name);
		~Rabbitmq();
		bool sendData(uint8_t* data, size_t len);

	private:

	private:
		std::string m_exchange;
		std::string m_queue_name;
		std::string m_connect_string;
		AMQP* p_amqp = nullptr;
		AMQPExchange* p_exchange = nullptr;
	};
}
