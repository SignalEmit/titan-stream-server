#include "threadbase.h"
namespace Thread
{
	ThreadBase::ThreadBase():m_running(false)
	{

	}

	ThreadBase::~ThreadBase()
	{
		this->stop();
	}

	bool ThreadBase::start()
	{

		if (p_thread != nullptr)
		{
			return false;
		}

		m_running = true;
		auto func = std::bind(&ThreadBase::run, this);
		p_thread = new std::thread(func);

		return true;
	}

	bool ThreadBase::stop()
	{
		if (p_thread != nullptr)
		{
			m_running = false;
			p_thread->join();
			delete p_thread;
			p_thread = nullptr;
		}

		return true;
	}

	
}