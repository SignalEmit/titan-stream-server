#include "file.h"
// 定义一些个系统相关的一些接口
namespace File
{
	File::File(std::string filepath)
	{
		m_filepath = filepath;
	}

	 File::~File()
	{
		  
	}

	 // 打开文件
	 bool File::isExist( )
	 {
		 std::ifstream f(m_filepath.c_str());
		 return f.good();
	 }

	 std::string File::readAll()
	 {
		 std::string str;
		 std::ifstream ifile(m_filepath.data(), std::ifstream::in | std::ifstream::binary);
		 std::stringstream buffer;
		 buffer << ifile.rdbuf();
		 str = buffer.str();

		 ifile.close();
		 return str;
	 }
}