#include "log.h"

bool Log::m_print_log = false;

Log::Log()
{

}

Log::~Log()
{

}

void Log::print(std::string str)
{
	if (m_print_log)
	{
		std::cout << str << std::endl;
	}
}

void Log::printInfo(std::string str)
{
	if (m_print_log)
	{
		std::cout << Date::Date::getNowTime() << str << std::endl;
	}
}

void Log::printError(std::string str)
{
	if (m_print_log)
	{
		std::cout << Date::Date::getNowTime() << str << std::endl;
	}
}

void Log::printDebug(std::string str)
{
	if (m_print_log)
	{
		std::cout << Date::Date::getNowTime() << str << std::endl;
	}
}

bool Log::setPrint(bool state)
{
	m_print_log = state;
	return true;
}