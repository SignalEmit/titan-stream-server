#include "httptype.h"
namespace Http
{
	std::map< HTTPSTATECODE, std::string> HttpTypeManger::m_http_code_map;
	bool HttpTypeManger::setAllCode()
	{
		m_http_code_map[HTTP_STATE_OK]			= "200 OK";
		m_http_code_map[HTTP_STATE_NOTFOUND]	= "404 Not Found";
		return true;
	}

	
	bool HttpTypeManger::getStateString(HTTPSTATECODE code, std::string &strcode)
	{ 
		auto it = m_http_code_map.find(code);
		if (it != m_http_code_map.end())
		{
			strcode = it->second;
			return true;
		}
		else
		{
			return false;
		}
		return false;
	}
}