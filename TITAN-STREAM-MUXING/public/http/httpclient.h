#pragma once
#include <string>
#include <functional>
#include "./common/mongoose.h"
#include "./log.h"
#include <atomic>
#include <map>
#include "httptype.h"

namespace Http
{
	// 此处必须用function类，typedef再后面函数指针赋值无效
	using reqCallback = std::function<void(char *data,size_t len)>;

	class HttpClient
	{ 
	public:
		HttpClient(std::string url, std::map<Http::HTTP_HEAD_KEY, Http::HTTP_HEAD_VALUE> headers);
		HttpClient(std::string url);
		~HttpClient();
		bool send( char *data, size_t len, reqCallback req_callback = nullptr );
	protected: 
		static void replay(mg_connection *connection, int event_type, void *event_data);
	private:
		std::string m_url;
		reqCallback p_req_callback = nullptr;

		// 退出标志 true:退出  false:不退出
		std::atomic<bool> m_exit;

		std::map<Http::HTTP_HEAD_KEY, Http::HTTP_HEAD_VALUE> m_headers;
	};
}

