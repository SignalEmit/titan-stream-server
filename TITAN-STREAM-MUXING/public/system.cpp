#include "system.h"

#ifdef _WIN32
// windows相关
#include <windows.h>
#else
// linux相关
#include <unistd.h>
#endif
// 定义一些个系统相关的一些接口
namespace System
{

	// wchar_t to string
	void Wchar_tToString(std::string& szDst, wchar_t* wchar)
	{
		wchar_t* wText = wchar;
		DWORD dwNum = WideCharToMultiByte(CP_OEMCP, NULL, wText, -1, NULL, 0, NULL, FALSE);//WideCharToMultiByte的运用
		char* psText;  // psText为char*的临时数组，作为赋值给std::string的中间变量
		psText = new char[dwNum];
		WideCharToMultiByte(CP_OEMCP, NULL, wText, -1, psText, dwNum, NULL, FALSE);//WideCharToMultiByte的再次运用
		szDst = psText;// std::string赋值
		delete[]psText;// psText的清除
	}

	std::string Path::getRunningPath()
	{
		std::string path;
		wchar_t  buff[256] = { 0 };
		

#if _WIN32
		GetModuleFileName(NULL, buff, 256);
		Wchar_tToString(path, buff);
#else
		readlink("/proc/self/exe", buff, 256);
		path = buff;
#endif
		
		path = path.substr(0, path.rfind(getSubSymbol()))+ getSubSymbol();
		return path;
	} 

	std::string Path::getSubSymbol()
	{
#if _WIN32
		return "\\";
#else
		return "/";
#endif
	}
}