#pragma once
#include <string>
#include <iostream>
#include "date.h"
#include "./http/httpserver.h"
class HLSServer
{
public:
	static HLSServer* getServer();
	bool listen(Http::http_server_reply reply);
private:
	HLSServer();
	~HLSServer();
	Http::HttpServer m_http_server;

};